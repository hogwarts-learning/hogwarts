# -*- coding: utf-8 -*-
# test file
import sys

import pytest

print(sys.path.append('..'))

from pythoncode.calc import Calculator


@pytest.mark.add
def test_add():
    calc = Calculator()
    assert 2 == calc.add(1, 1)


@pytest.mark.add
def test_add_one():
    calc = Calculator()
    assert 8 == calc.add(7, 1)


@pytest.mark.div
def test_div():
    calc = Calculator()
    assert 4 == calc.div(0, 2)
