class Calculator:

    # 加法运算
    def add(self, a, b):
        return a + b

    # 除法运算
    def div(self, a, b):
        if a == 0:
            return 0
        elif b == 0:
            return 0
        else:
            return a / b

    # 乘法运算
    def mul(self, a, b):
        return a * b

    # 减法运算
    def subt(self, a, b):
        return a - b
