from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage


class ContactDetailBriefInfoProfilePage(BasePage):
    settting_id = (MobileBy.ID, "com.tencent.wework:id/h9p")

    def goto_contact_detail_setting(self):
        self.find_and_click(self.settting_id)
        from mobile.page_object.contact_detail_setting_page import ContactDetailSettingPage
        return ContactDetailSettingPage(self.driver)
