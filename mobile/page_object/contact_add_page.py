from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage


class ContactAddPage(BasePage):
    # def __init__(self, driver):
    #     self.driver = driver

    name_element = (MobileBy.XPATH,
                    "//*[contains(@text, '姓名')]/..//*[@class='android.widget.EditText']")
    gender_element = (MobileBy.XPATH,
                      "//*[contains(@text, '性别')]/..//*[@text='男']")
    male_element = (MobileBy.XPATH, "//*[@text='男']")
    female_element = (MobileBy.XPATH, "//*[@text='女']")
    phone_element = (MobileBy.XPATH, "//*[@text='手机号']")
    save_element = (MobileBy.XPATH, "//*[@text='保存']")

    def set_name(self, name):
        # 设置姓名
        self.find_and_sendkeys(self.name_element, name)
        return self

    def set_gender(self, gender):
        # 设置性别
        self.find_and_click(self.gender_element)
        if gender is '男':
            self.find_and_click(self.male_element)
        else:
            self.find_and_click(self.female_element)
        return self

    def set_phone(self, phonenum):
        # 设置手机号
        self.find_and_sendkeys(self.phone_element, phonenum)
        return self

    def click_save(self):
        from mobile.page_object.add_member_page import AddMemberPage
        # 点击保存
        self.find_and_click(self.save_element)

        return AddMemberPage(self.driver)
