# ！/usr/env python
# -*- coding: utf-8 -*-

'''
通讯录
'''
from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.add_member_page import AddMemberPage
from mobile.page_object.base_page import BasePage


class ContactListPage(BasePage):
    # def __init__(self, driver):
    #     self.driver = driver

    addmember_text = "添加成员"
    search_id = (MobileBy.ID, "com.tencent.wework:id/h9z")

    def add_contact(self):
        """
        添加用户
        :return:
        """
        # self.driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,
        #                          'new UiScrollable(new UiSelector()'
        #                          '.scrollable(true).instance(0))'
        #                          '.scrollIntoView(new UiSelector()'
        #                          '.text("添加成员").instance(0));').click()
        self.find_by_scroll(self.addmember_text).click()
        return AddMemberPage(self.driver)

