from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage

'''
搜索页面
'''


class SearchPage(BasePage):
    search_text = (MobileBy.XPATH, "android.widget.EditText[@text='搜索']")

    def search_member(self, name, type=2):
        self.find_and_sendkeys(self.search_text, name)
        ele = self.find_and_click((MobileBy.XPATH, f'//*[@text="{name}"]'))
        if type is 2:
            if len(ele) < 2:
                print("查无此人")
                return
            ele.click()
            from mobile.page_object.contact_detail_brief_info_profile_page import ContactDetailBriefInfoProfilePage
            return ContactDetailBriefInfoProfilePage(self.driver)
        if type is 1:
            return ele
