# ！/usr/env python
# -*- coding: utf-8 -*-


"""
企业微信主页
"""
from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage
from mobile.page_object.contact_list_page import ContactListPage
from mobile.page_object.search_page import SearchPage


class MainPage(BasePage):
    #    def __init__(self, driver):
    #        self.driver = driver

    contactlist = (MobileBy.XPATH, "//*[@text='通讯录']")

    def goto_contact_list(self):
        """
        进入通讯录
        :return:
        """
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        self.find_and_click(self.contactlist)
        return ContactListPage(self.driver)

    def seach_contact(self):
        """
        进入搜索
        :return:
        """
        self.find_and_click(self.search_id)
        return SearchPage(self.driver)

    def goto_workbench(self):
        """
        进入工作台
        :return:
        """
        pass

    def goto_me(self):
        """
        进入个人中心
        :return:
        """
        pass

    def goto_add(self):
        """
        进入添加功能
        :return:
        """
        pass
