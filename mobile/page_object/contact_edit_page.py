from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage


class ContactEditPage(BasePage):
    del_text = (MobileBy.XPATH, "//*[@text='删除成员']")
    define_text = (MobileBy.XPATH, "//*[@text='确定']")

    def delete_member(self):
        self.find_and_click(self.del_text)
        self.find_and_click(self.define_text)

        from mobile.page_object.search_page import SearchPage
        return SearchPage(self.driver)
