from appium.webdriver.common.mobileby import MobileBy

from mobile.page_object.base_page import BasePage


class ContactDetailSettingPage(BasePage):
    edit_member = (MobileBy.XPATH, "//*[@text='编辑成员']")

    def edit_member(self):
        self.find_and_click(self.edit_member)
        from mobile.page_object.contact_edit_page import ContactEditPage
        return ContactEditPage(self.driver)
