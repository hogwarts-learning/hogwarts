# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
from time import sleep

import allure
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction

"""

"""


class TestWework:
    def setup(self):
        caps = {}
        caps["platformName"] = "android"
        caps["deviceName"] = "emulator-5554"
        caps["appPacckage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.WwMainActivity"
        caps["dontStopAppOnReset"] = "true"
        caps["skipDeviceInitialization"] = "true"
        caps["unicodeKeyBoard"] = "true"

        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        self.driver.quit()

    @allure.story("打卡")
    def test_case(self):
        self.driver.find_element(MobileBy.XPATH, "//*[@text='工作台']").click()

        action = TouchAction(self.driver)
        window_rect = self.driver.get_window_rect()
        width = window_rect['width']
        height = window_rect['height']
        x_start = int(width * 0.5)
        start_y = int(height * 0.95)
        end_y = int(height * 0.1)
        action.press(x=x_start, y=start_y).wait(200).move_to(x=x_start, y=end_y).release().perform()

        self.driver.find_element_by_xpath("//*[@text='打卡']").click()
        self.driver.find_element_by_id("com.tencent.wework:id/gw8").click()
        self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/ao_").click()

    @allure.story("添加成员")
    def test_wework(self):
        with allure.step("点通讯录"):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()

        with allure.step("添加成员"):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成员']").click()

        with allure.step("手动输入添加"):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()

        with allure.step("输入用户名称"):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='必填']").send_keys("Mr Shan")
        with allure.step("输入手机号"):
            self.driver.find_element_by_id("com.tencent.wework:id/f1e").send_keys("15710090800")
        with allure.step("设置部门"):
            self.driver.find_element_by_xpath("//*[@text='设置部门']").click()
        with allure.step("部门确认"):
            self.driver.find_element_by_id("com.tencent.wework:id/g09").click()
        with allure.step("用户保存"):
            self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/h9w").click()
        sleep(5)
        with allure.step("返回通讯录"):
            self.driver.find_element_by_id("com.tencent.wework:id/h9e").click()
        with allure.step("添加成功"):
            print("你真棒👍 ")

    @allure.story("删除成员")
    def test_delete_user(self):
        self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        #        self.driver.find_element_by_id("com.tencent.wework:id/h9z").click()
        #        self.driver.find_element(MobileBy.XPATH,"//*[@text='搜索']").send_keys("Mr Shan")
        sleep(3)
        self.driver.find_element(MobileBy.XPATH, "//*[@text='Mr Shan']").click()
        self.driver.find_element_by_id("com.tencent.wework:id/h9p").click()
        self.driver.find_element_by_id("com.tencent.wework:id/b2c").click()

        action = TouchAction(self.driver)
        window_rect = self.driver.get_window_rect()
        width = window_rect['width']
        height = window_rect['height']
        x_start = int(width * 0.5)
        start_y = int(height * 0.95)
        end_y = int(height * 0.1)
        action.press(x=x_start, y=start_y).wait(200).move_to(x=x_start, y=end_y).release().perform()

        self.driver.find_element_by_id("com.tencent.wework:id/e3f").click()
        self.driver.find_element_by_id("com.tencent.wework:id/bci").click()
