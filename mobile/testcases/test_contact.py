import pytest

from mobile.page_app.app import App
import yaml


class TestContact:
    with open('../datas/addcontact.yml') as f:
        addcontactdatas = yaml.safe_load(f)

    with open('../datas/delcontact.yml') as f:
        delcontactdatas = yaml.safe_load(f)

    def setup_class(self):
        self.app = App()

    def teardown_class(self):
        self.app.stop()

    def setup(self):
        self.main = self.app.start().goto_main()

    def teardown(self):
        self.app.back(5)

    @pytest.mark.parametrize('name,gender,phonenum',
                             addcontactdatas)
    def test_addcontact(self, name, gender, phonenum):
        """
        添加联系人
        :return:
        """
        # name = "Mr Shan1"
        # gender = "女"
        # phone_num = "12290909090"
        mypage = self.main.goto_contact_list(). \
            add_contact().add_manual(). \
            set_name(name).set_gender(gender).set_phone(phonenum).click_save()
        text = mypage.get_toast()
        assert '成功' in text
        self.app.back()

    @pytest.mark.parametrize('name', delcontactdatas)
    def test_delcontact(self, name):
        self.main.seach_contact(). \
            search_member(name, 5). \
            goto_contact_detail_setting(). \
            edit_member(). \
            delete_member()
