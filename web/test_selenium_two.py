import shelve
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By


class Test():

    def setup(self):
        chrome_options = Options()
        # 和浏览器打开的调试端口进行通信
        # chrome --remote-debugging-port=9222 开启调试
        chrome_options.debugger_address = "127.0.0.1:9222"
        self.driver = webdriver.Chrome()

    def test_baidu(self):
        self.driver.get("https://www.baidu.com/")
        self.driver.find_element(By.ID, "kw").send_keys("霍格沃兹测试学院")
        self.driver.find_element(By.ID, "su").click()
        time.sleep(3)
        self.driver.find_element(By.LINK_TEXT, "霍格沃兹测试学院 – 测试开发工程师的黄埔军校").click()

    def test_1(self):
        self.driver.find_element(By.ID, "menu_contacts").click()
        print(self.driver.get_cookies())

    def test_wework(self):

        self.driver.get("https://work.weixin.qq.com/")
        db = shelve.open("cookies")
        # 将数据存储到数据库
        # db["cookies"] = cookies
        # db.close()
        # 读取数据库 cookies
        cookies = db["cookies"]
        for cookie in cookies:
            if "expiry" in cookie.keys():
                cookie.pop("expiry")
            # 把字典添加cookie中
            self.driver.add_cookie(cookie)
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        time.sleep(5)
        self.driver.find_element(By.ID, "menu_contacts").click()
        db.close()
