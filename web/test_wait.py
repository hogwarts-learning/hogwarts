import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestWait:
    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://ceshiren.com/")
        self.driver.implicitly_wait(3)

    def teardown(self):
        pass

    def test_wait(self):
        self.driver.find_element(By.XPATH, '//*[@title="有了新帖的活动主题"]').click()

        #       def wait(x):
        #           return len(self.driver.find_elements(By.XPATH, '//*[@class="nav nav-pills ember-view"')) >= 1
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable((By.XPATH, '//*[@class="ember-view"')))
        #        time.sleep(3)
        self.driver.find_element(By.XPATH, '//*[@title="招聘内推"]').click()

        print("hello")
