from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class Register:
    """
    注册页面的PO
    """

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def register(self):
        """
        输入内容
        点击下拉框
        :return:
        """
        self.driver.find_element(By.CSS_SELECTOR, "#corp_name").send_keys("杭州01科技有限公司")
        self.driver.find_element(By.CSS_SELECTOR, "#manager_name").send_keys("MrShan")
        return True
