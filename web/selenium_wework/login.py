from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By

from web.selenium_wework.register import Register


class Login:
    """
    登录页面的PO
    """

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def scan(self):
        """
        扫码二维码
        :return:
        """
        pass

    def goto_regidter(self):
        """
        点击企业注册
        进入企业注册的PO
        :return:
        """
        self.driver.find_element(By.CSS_SELECTOR, ".login_registerBar_link").click()
        return Register(self.driver)
