from time import sleep

from selenium.webdriver.common.by import By

from web.test_wework.add_member import AddMember
from web.test_wework.base_page import BasePage


class Index(BasePage):
    """
    #用户首页
    """
    _base_url = "https://work.weixin.qq.com/wework_admin/frame#index"

    def goto_add_member(self):
        """
        添加成员
        :return:
        """

        def add_member_condition(x):
            """
            改写显示等待条件
            :param x:
            :return:
            """
            element_len = len(self.finds(By.ID, "username"))
            if element_len <= 0:
                self.find(By.CSS_SELECTOR, ".js_has_member>div:nth-child(1)>a:nth-child(2)").click()
            # 如果username不存在，就会触发until中的列循环
            return element_len > 0

        self.find(By.CSS_SELECTOR, "#menu_contacts").click()
        #        self.wait_for_click((By.CSS_SELECTOR, ".js_has_member>div:nth-child(1)>a:nth-child(2)"))
        self.wait_for_condition(add_member_condition)

        return AddMember(self._driver)

    def goto_import_address(self):
        """
        导入通讯录
        :return:
        """
        pass

    def goto_join_member(self):
        """
        成员加入
        :return:
        """
        pass

    def goto_group_message(self):
        """
        消息群发
        :return:
        """
        pass

    def goto_contact_clients(self):
        """
        客户联系
        :return:
        """
        pass

    def goto_clock_in(self):
        """
        打卡
        :return:
        """
        pass
