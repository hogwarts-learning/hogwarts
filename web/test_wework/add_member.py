from selenium.webdriver.common.by import By

from web.test_wework.base_page import BasePage


class AddMember(BasePage):
    """
    添加成员PO
    """

    def add_member(self):
        self.find(By.ID, "username").send_keys("少韩")
        self.find(By.ID, "memberAdd_english_name").send_keys("少韩")
        self.find(By.ID, "memberAdd_acctid").send_keys("MrShan666888")
        self.find(By.ID, "memberAdd_phone").send_keys("15394210936")
        self.find(By.CSS_SELECTOR, ".js_btn_save").click()
